<?php
/**
 * @package Hello_Dolly
 * @version 1.7.2
 */
/*
Plugin Name: Hello Dolly
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Matt Mullenweg
Version: 1.7.2
Author URI: http://ma.tt/
*/



function displayTab(){
	global $wpdb;
	$array = $wpdb->get_results("SELECT * FROM wp_annuaire");

	$data = '<table cellpadding="2" cellspacing="2" border="2"><thead><tr>';
	foreach($array[0] as $k => $v){
		$data.= '<th>'.$k.'</th>';
	}
	$data.= '</tr></thead><tbody>';
	foreach($array as $v){
		$data.= '<tr>';
		foreach($v as $v2){
			$data.= '<td>'.$v2.'</td>';
		}
		$data.= '</tr>';
	}
	$data.= '</tbody></table>';

	return $data;
}

add_shortcode('showTab', 'displayTab'); ?>