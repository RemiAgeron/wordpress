CREATE TABLE `wp_annuaire` (
  `id` int(11) NOT NULL,
  `nom_entreprise` varchar(100) NOT NULL,
  `localisation_entreprise` varchar(100) NOT NULL,
  `prenom_contact` varchar(100) NOT NULL,
  `nom_contact` varchar(100) NOT NULL,
  `mail_contact` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `wp_annuaire` (
  `id`, 
  `nom_entreprise`, 
  `localisation_entreprise`, 
  `prenom_contact`, 
  `nom_contact`, 
  `mail_contact`
) VALUES
  (1, 'Supernova', 'Toulouse', 'Marcel', 'Rodriguez', 'contact@rodrigo.com'),
  (2, 'IndieTroll', 'Carbonne', 'Michel', 'Tournier', 'lastar@planetaire.org'),
  (3, 'Laboulette', 'Saint Gaudens', 'Julie', 'Lila', 'lilali@lololu.net')
;
