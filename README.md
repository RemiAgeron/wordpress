# Site Wordpress : Annuaire des entreprises de votre réseau pro

## Pré-requis :
    WAMP
    PHPmyadmin

## Installation

1. Après avoir installé WAMP :

Ouvrir PHPmyadmin et dans une base de données que vous pouvez nommer **wp_DB**.
Copier le contenu et lancer le script SQL qui est présent dans le répertoire **wordpress**, dans le fichier :

```
ScriptBDD.txt
```

2. Pour installer le serveur Wordpress :

Cloner le dossier dans le répertoire :

```
C:\wamp64\www
```

3. Ouvrir le navigateur, à l'adresse :

```
http://localhost/wordpress
```

4. Configuration Wordpress

Configurer la base de données en donnant le nom que vous avez précisez à l'étape 1, puis le mot de passe de l'administrateur.

Vous pouvez maintenant profitez de votre site internet !!!
